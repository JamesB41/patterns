#include <iostream>
#include <iomanip>
#include <cmath>
#include <string>
#include <stdlib.h>

using namespace std;

void displayMenu();
void program();
int getSelection();
int getSize();
void drawPattern(int, int);
void writeLine(string="");
void handleMenuSelection(int);

int main()
{
	displayMenu();

	while (1 == 1)
	{
		program();
	}

	return 0;
}

void displayMenu()
{
	writeLine("------------------------------");
	writeLine("        PATTERN MAKER!");
	writeLine("------------------------------");
	writeLine("One\t Two\t Three\t Four");
	writeLine("5$$$$\t $$$$5\t $$$$$\t $$$$$");
	writeLine("$5$$$\t $$$5$\t $$$$5\t 5$$$$");
	writeLine("$$5$$\t $$5$$\t $$$55\t 55$$$");
	writeLine("$$$5$\t $5$$$\t $$555\t 555$$");
	writeLine("$$$$5\t 5$$$$\t $5555\t 5555$");
	writeLine("------------------------------");
	writeLine("1. Pattern One");
	writeLine("2. Pattern Two");
	writeLine("3. Pattern Three");
	writeLine("4. Pattern Four");
	writeLine("5. Run Again");
	writeLine("6. Quit");
	writeLine("-----------------------------");
	writeLine();
}

void program()
{
	int menu_selection, size_selection;

	menu_selection = getSelection();
	handleMenuSelection(menu_selection);
	size_selection = getSize();
	drawPattern(menu_selection, size_selection);
}

int getSelection()
{
	int menu_selection;
	cout << "Please enter your selection: ";
	cin >> menu_selection;

	while (menu_selection < 1 || menu_selection > 6)
	{
		writeLine("Invalid selection. Please enter 1-6.");
		cout << "Enter your selection: ";
		cin >> menu_selection;
	}

	return menu_selection;
}

int getSize()
{
	int size_selection;

	cout << "Select a size 2 to 9: ";
	cin >> size_selection;
	while (size_selection < 2 || size_selection > 9)
	{
		writeLine("Invalid selection. Please enter 2-9.");
		cout << "Select a size 2 to 9: ";
		cin >> size_selection;
	}
	writeLine("You have selected size " + to_string(size_selection));

	return size_selection;
}

void handleMenuSelection(int menu_selection)
{
	switch (menu_selection)
	{
		case 5:
			main();
			break;
		case 6:
			writeLine();
			writeLine("Thanks for playing!");
			exit(0);
			break;
		default:
			writeLine("You have selected pattern number " + to_string(menu_selection));
			break;
	}
}

void drawPattern(int menu_selection, int size_selection)
{
	int row, col;
	char size = (char)size_selection + '0';
	char fill = '$';

	writeLine();
	writeLine("Your pattern is:");
	writeLine();

	for (row = 1; row <= size_selection; row++)
	{
		for (col = 1; col <= size_selection; col++)
		{
			switch(menu_selection)
			{
				case 1:
					cout << ((col == row) ? size : fill);
					break;
				case 2:
					cout << ((col + row - 1 == size_selection) ? size : fill);
					break;
				case 3:
					cout << ((row + col > (size_selection + 1)) ? size : fill);
					break;
				case 4:
					cout << ((row > col) ? size : fill);
					break;
			}
		}
		writeLine();
	}

	writeLine();
}

void writeLine(string line)
{
	cout << line << endl;
}
